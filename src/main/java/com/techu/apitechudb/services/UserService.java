package com.techu.apitechudb.services;

import com.techu.apitechudb.models.UserModel;
import com.techu.apitechudb.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class UserService {

    @Autowired
    UserRepository userRepository;

    public List<UserModel> findAll(int ageFilter) {
        System.out.println("findAll en UserService");

        if (ageFilter <= 0) {
            return this.userRepository.findAll();
        }
        System.out.println("ageFilter es " + ageFilter);

        ArrayList<UserModel> result = new ArrayList<>();

        for (UserModel userInList : this.userRepository.findAll()) {
            if (userInList.getAge() == ageFilter) {
                System.out.println("Usuario " + userInList.getName() + " tiene la misma edad que el filtro.");
                result.add(userInList);
            }
        }

        return result;
    }

    public UserModel add(UserModel user) {
        System.out.println("add en UserService");

        return this.userRepository.save(user);
    }

    public Optional<UserModel> findById(String id) {
        System.out.println("findById en UserService");
        System.out.println("La id es " + id);

        return this.userRepository.findById(id);
    }

    public UserModel update(UserModel user) {
        System.out.println("update en UserService");

        return this.userRepository.update(user);
    }

    public boolean delete(String id) {
        System.out.println("delete en UserService");
        System.out.println("La id del usuario a borrar es " + id);

        boolean result = false;

        Optional<UserModel> userToDelete = this.findById(id);

        if (userToDelete.isPresent() == true) {
            System.out.println("Usuario a borrar encontrado");
            result = true;
            this.userRepository.delete(userToDelete.get());
        }

        return result;
    }
}

